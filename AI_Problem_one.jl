### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ 422c6542-db60-4a4c-a98e-a4dc6c84b769
begin 
	using Pkg
	Pkg.activate("project.toml")
	using PlutoUI
end

# ╔═╡ 2c1ec482-859a-46de-9abe-2569d8db5dda
### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ 6e8b44f7-1b2f-40d0-a957-6bf77a9dd959
begin
	mutable struct office
		Id::String
		itemCount::Int64
		neighbourW::Bool
		neighbourE::Bool
		index::Int64
	end
	
	struct Action
		Name::String
		Cost::Int64
	end
	
   mutable struct location
		current_O::office
		prev_O::office
		prev_A::Action
		#next_O::office
		
	end	
	
	mutable struct state 
		I_Count::Int64
		S_Cost::Int64
		office_Lot::Vector{Int64}
		A_position::Int64
		huristicVal::Int64
	end
	
	WO 	= office("West_Office", 1, false,true,1)
	B01 	= office("B01_Office", 3, true,true,2)
	B02 	= office("B02_Office", 2, true,true,3)
	EO 	= office("East_Office", 1, true,false,4)
	
	MU = Action("mu",1)
	MD = Action("md",1)
	ME = Action("Move East", 2)
	MW = Action("Move West", 2)
	Co = Action("Collect", 5)
	
	global Current_position = location(C1,C1,Co)
	global evaluate = true
	
	ItemCount = 7
	officeList = [WO,B01,B02,EO]
	TransitionModel = Dict()
	moves = []
	
	availableActions = Action[MU,MD,ME,MW,Co]
	currentBalance = 5
	stateList = []
	stuffInOffices = [1,1,1,1]
	
	# global c_State = state(ItemCount,currentBalance,stuffInOffices,Current_position.current_O.index, 0)
	global c_State = state(6, 5, [1,1,1,1],2,11) #
	global nodeList = state[]
	global closedList = state[]
	
	
	
end

# ╔═╡ ff220984-4000-41df-880e-4e17f9c20155
#= if Current_position.prev_A != Re && current_O.itemCount != 0
		push!(actionQue, Re)
end
if Current_position.prev_A != Co && current_O.itemCount != 0 =#

# ╔═╡ b5dc583f-51e8-4216-b6c4-9c53f3f5301c
#=
function move(Action, Old_Location)
	#	currentBalance != 0 #Wont run if No Moves made yet
	
	#Move West
		if Action.Name == "Move West" && Old_Location.current_O.neighbourW == true
#=	
		if Old_Location.prev_A == Me && Old_Location.prev_O.itemCount == 0 
			push!(moves, "Usless Move West")
			return
		end
		
=#
		index = Old_Location.current_O.index #Record Index
		
		Old_Location.prev_O = officeList[index] #Record Office Being Left
		
		Old_Location.prev_A = Action #Record Action taken
		
		Old_Location.current_O = officeList[index - 1] #Record Current 
		
		Current_position = Old_Location # Change Location
		
		currentBalance.value = currentBalance.value + Action.Cost #add Cost
		
		push!(moves, Action.Name) #Track movement
		
		#Build New State
		
		#push!(moves, string("New Cost ", currentBalance.value))
		return
	elseif Action.Name == "Move West" && Old_Location.current_O.neighbourW == false
	push!(moves, string("West Move Not Allowed => ", Old_Location.current_O.index))
		return
end
	#Move East
		if Action.Name == "Move East" && Old_Location.current_O.neighbourE == true
#=
		if Old_Location.prev_A == Mw && Old_Location.prev_O.itemCount == 0 Old_Location.current_O.neighbourW == false
			push!(moves, "Usless Move East")
			return
		end
=#
		index = Old_Location.current_O.index
		Old_Location.prev_O = officeList[index]
		Old_Location.prev_A = Action
		Old_Location.current_O = officeList[index + 1]
		Current_position = Old_Location
		currentBalance.value = currentBalance.value + Action.Cost
		push!(moves, Action.Name) #Track movement
		#push!(moves, string("New Cost ", currentBalance.value))
		return

	elseif Action.Name == "Move East" && Old_Location.current_O.neighbourE == false
	push!(moves, string("East Move Not Allowed => ", Old_Location.current_O.index))
		return
end
	
	
	#Collect Action
		if Action.Name == "Collect"
	
		if  Old_Location.prev_A == Co
			push!(moves, "Cannot Collect Twice")
			return
	end
		if Old_Location.current_O.itemCount == 0
			push!(moves, "Office Is Empty")
			return
	end				
				    #If West Office
		if  Wo == Old_Location.current_O
			 Wo.itemCount = Wo.itemCount - 1;
				return
				end
					#If C1 Office
		if C1 == Old_Location.current_O
		     	C1.itemCount = C1.itemCount - 1;
				return
				end 
					#If C2 Office
		if C2 == Old_Location.current_O
				 C2.itemCount = C2.itemCount - 1;
				return
				end		
					#If East Office
		if E == Old_Location.current_O
				 E.itemCount = E.itemCount - 1;
				return
				end			
			
	#Remain Action
		if Action.Name == "Remain"
			
		if Old_Location.current_O.itemCount == 0
			push!(moves, "Cannot Remain In Empty Office")
			return
	end		
		if  Old_Location.prev_A == Re
			push!(moves, "Cannot Remain Twice")
			return
	end
			
				    #If West Office
		if  Wo == Old_Location.current_O
			 Wo.itemCount = Wo.itemCount - 1;
				return
				end
					#If C1 Office
		if C1 == Old_Location.current_O
		     	C1.itemCount = C1.itemCount - 1;
				return
				end 
					#If C2 Office
		if C2 == Old_Location.current_O
				 C2.itemCount = C2.itemCount - 1;
				return
				end		
					#If East Office
		if E == Old_Location.current_O
				 E.itemCount = E.itemCount - 1;
				return
				end	
			
		#=			
		push!(moves, "1 Item Collected")
		
		index = Old_Location.current_O.index
		Old_Location.prev_O = officeList[index]
		Old_Location.prev_A = Action
		Old_Location.current_O = officeList[index + 1]
		Current_position = Old_Location
		currentBalance.value = currentBalance.value + Action.Cost
		push!(moves, Action.Name) #Track movement
		#push!(moves, string("New Cost ", currentBalance.value))
		return
	elseif Action.Name == "Move East" && Old_Location.current_O.neighbourE == false
	push!(moves, string("East Move Not Allowed =>", Old_Location.current_O.index))
		return	
			=#	
			
		end
		end
	end
=#

# ╔═╡ e4386126-392d-425d-b22d-aa31cfb45d69
# move()

# ╔═╡ ce1895fb-3816-4f17-9fe2-20a480b70a05
function StateTracker(H_Val, C_Val, office_Lot, A_position)
	if evaluate != true 
		N_state = state(H_Val,C_Val )
		push!(nodeList, N_state)
	
	end
end

# ╔═╡ cd8f1cd7-c636-475f-9abf-72e2fcf2a9f7
# function get_Heuristic(ItemCount, totalCost, action)
# 	if action == Co 
# 		x = Item
# end

# ╔═╡ d3561084-b63e-4010-bc01-a9af6b0ce415
begin
	Cr = state(6, 6, [2,2,2,2],2,12) #
	go_w = state(6, 8, [2,2,2,2],1,14) #agent to go westward
	go_e = state(6, 8, [2,2,2,2],3,14) #agent to move eastward
end

# ╔═╡ a56da88d-fe30-443d-b000-071b464f5e8e
begin 
	push!(nodeList, Cr)
	push!(nodeList, go_w)
	push!(nodeList, go_e)
end

# ╔═╡ 0c3405fd-eaa6-4e0d-93e8-fc94ce35db23
with_terminal() do
	println(nodeList)
end

# ╔═╡ ffe1431d-44ed-4fb2-812e-49c57bb8dbb1
# for node in nodeList
# 	if openList(node)
# 		# show()
# 		return
# end

# ╔═╡ 15b6d80b-dcc9-4bab-bd0d-b50bb64ea693
function evaluateOpenList(c_State)
	T_cost = currentBalance
	T_ItemCount = c_State.I_Count
         if evaluate == true
		
		for node in nodeList
			
			if node.I_Count != T_ItemCount
				
				if node.I_Count < T_ItemCount
					push!(closedList, node)
					 c_State = node
					push!(moves, node)
					return
				end
				
			end
			
		end
		
		for node in nodeList
		push!(moves, node.I_Count == T_ItemCount)
			if node.I_Count == T_ItemCount
						push!(moves,string("h=>",node.huristicVal, "h2=>",T_cost))
				if node.huristicVal <= T_Hval
					T_Hval = node.huristicVal
					 c_State = node
					push!(closedList, node)
					push!(moves, "low")
					return
				end
				
			end
			
		end
		
		
		
		
	end
end

# ╔═╡ ab785296-e53b-4d43-ae9c-c99ffb73ee7c
function openList(aState)

	if evaluate == false
		push!(nodeList, N_state)
	return true
	elseif evalute == true
		evaluateOpenList()
	end
end

# ╔═╡ 2949aa02-4b6b-49d5-acfd-cb1847fe29b6
#=
begin
	move(Mw,Current_position)
	move(Mw,Current_position)
	move(Me,Current_position)
	move(Me,Current_position)
	move(Me,Current_position)
	move(Me,Current_position)
end
=#	
#move(Mw,Current_position)
	#move(Me,Current_position)
	#move(Co,Current_position)
	#move(Re,Current_position)
#=
#wont work here
with_terminal() do
   println(moves)
end=#

# ╔═╡ b0f5ae21-fc63-47ce-872b-c2965e04ff7b
#=with_terminal() do
		println(officeList)
end=#

# ╔═╡ 559c1183-d067-4917-b1d6-960d4e21cead
# with_terminal() do 
# 	println(string(
# 			"Action Taken => ",  moves,"\n",
# 			"Location => ",Current_position.current_O.Id, "\n",
# 			"Previous Node => ", Current_position.prev_O.Id,"\n",
# 			"Previous Action => ", Current_position.prev_A.Name,"\n",
# 			"Office Arrangement =>", "\n",
# 			"Cost To Get Here => ", currentBalance))
# end

# ╔═╡ ad417b7e-9abf-4509-998a-6790f1676f09
classList = list(1,2,3)

# ╔═╡ 23f640b0-4b95-42f1-b5a8-2367351f4e47
function heuristic(cell_node, goal_node)
	return abs(cell_node[1] - goal_node[1]) + abs(cell_node[2] - goal_node[2])
end

# ╔═╡ b1b1721f-ed6c-43a1-9229-05ca2fa1874c
function astar(graph, start_node, goal_node)
	pr_queue = []
	push!(pr_queue, (0 + heuristic(start_node, goal_node), 0, "", start_node))
	visited = Set()
	
	while length(pr_queue) != 0
		_, cost, path, current = push!(pr_queue)
		
		if current == goal
			return path, push!(visited, current)
		end
		if current in visited
			continue
		end
		push!(visited, current)
		
		for (direction, neighbour) in graph[current]
			headpush!(pr_queue, (cost + heuristic(neighbour, goal), cost + 1, path*direction, neighbour))
		end
	end
	return "no such path"
end

# ╔═╡ 2bbfb355-4374-45df-941c-d674a32be9f4
begin
	R = state(7, 1, [1,3,2,1],2)
	
	
	begin
	
		start = state(7, 0, [1,3,2,1], 2)
	
		
		C = state(6, 5, [1,2,2,1],2) #11
		W = state(7, 3, [1,3,2,1],1) #10g
		E = state(7, 3, [1,3,2,1],3) #10
		
		
		Wc = state(6, 8, [0,3,2,1],1) #14
		Wr = state(7, 4, [1,3,2,1],1) #11
		We = state(7, 6, [1,3,2,1],2) #13
		
		Er = state(7, 4, [1,3,2,1],3) #11
		Ec = state(6, 8, [1,3,1,1],3) #14
		Ee = state(7, 6, [1,3,2,1],4) #13
		Ew = state(7, 6, [1,3,2,1],2) #13
		
		
		gr = state(6, 6, [1,2,2,1],2) #12 
		gw = state(6, 8, [1,2,2,1],1) #14
		ge = state(6, 8, [1,2,2,1],3) #14
		#done ^
		
		
		WrC = state(6, 9, [1,3,2,1],1) #15
		WrE = state(7, 7, [1,3,2,1],1) #14
		
		Er = state(7, 4, [1,3,2,1],3) #11
		Er = state(7, 4, [1,3,2,1],3) #11
		Er = state(7, 4, [1,3,2,1],3) #11
		Er = state(7, 4, [1,3,2,1],3) #11
		
		
		
		
		
		
		
		#Collect Action Branch
		#Cr = state(6, 6, [1,2,2,1],2) #12
		Cw = state(6, 8, [1,2,2,1],1) #14
		Ce = state(6, 8, [1,2,2,1],3) #14
	
		CrC = state(5, 11, [1,1,2,1],2) #16 =>
		CrW = state(6, 9,  [1,2,2,1],1) #15
		CrE = state(6, 9,  [1,2,2,1],3) #15
		
		CrCr = state(5, 12, [1,1,2,1],2) #17 =>
		CrCw = state(5, 14, [1,1,2,1],1) #19
		CrCe = state(5, 14, [1,1,2,1],3) #19
		
		CrCrC = state(4, 17, [1,0,2,1],2) #21 =>
		CrCrw = state(5, 15, [1,1,2,1],1) #20
		CrCre = state(5, 15, [1,1,2,1],3) #20
		
		CrCrCw = state(4, 20, [1,0,2,1],1) #24 * (who's first in line)
		CrCrCe = state(4, 20, [1,0,2,1],3) #24
		
		CrCrCwC = state(3, 25, [0,0,2,1],1) #28 =>
		CrCrCwR = state(4, 21, [1,0,2,1],1) #25
		CrCrCwE = state(4, 23, [1,0,2,1],2) #27
		
		
		CrCrCwCe = state(3, 28, [0,0,2,1],1) #31 => 
		CrCrCwCw = state(3, 28, [0,0,2,1],1) #31 Cant go west if neighbourA = F
		CrCrCwCr = state(3, 26, [0,0,2,1],1) #29 Cannot remain if Office = 0
		
		CrCrCwCeW = state(3, 31, [0,0,2,1],1) #29 check if Next_Office != Prev_Office
		CrCrCwCeE = state(3, 31, [0,0,2,1],3) #29 =>
		
		
		CrCrCwCeEc = state(2, 36, [0,0,1,1],3) #38 
		CrCrCwCeEr = state(3, 32, [0,0,2,1],3) #35 
		CrCrCwCeEe = state(3, 34, [0,0,2,1],3) #37 
		CrCrCwCeEw = state(3, 34, [0,0,2,1],3) #37 check if Next_Office != Prev_Office
		
		CrCrCwCeEcR = state(2, 37, [0,0,1,1],3) #39
		CrCrCwCeEcE = state(2, 39, [0,0,1,1],3) #41
		CrCrCwCeEcW = state(2, 39, [0,0,1,1],3) #41
		CrCrCwCeEcC = state(1, 41, [0,0,0,1],3) #42 Cannot Collect twice
		
		CrCrCwCeEcRc = state(1, 42, [0,0,0,1],3) #43 => 
		CrCrCwCeEcRe = state(2, 40, [0,0,1,1],3) #42
		CrCrCwCeEcRw = state(2, 40, [0,0,1,1],3) #42
		
		CrCrCwCeEcRcE = state(1, 45, [0,0,0,1],4) #46 => 
		CrCrCwCeEcRcW = state(1, 45, [0,0,0,1],2) #46 Preference to Office With Items
		CrCrCwCeEcRcR = state(1, 43, [0,0,0,1],3) #46 Agent Cannot stay in empty office or if Office = 0
		
		
		
		CrCrCwCeEcRcEc = state(0, 50, [0,0,0,0],4) #50 => Goalstate Reached, End successfully...
		
		CrCrCwCeEcRcEr = state(1, 46, [0,0,0,1],4) #47 
		CrCrCwCeEcRcEe = state(1, 48, [0,0,0,1],4) #49 => Cannot move westward if neighbourA = F
		CrCrCwCeEcRcEw = state(1, 48, [0,0,0,1],4) #48 
		
		
		
		CrCrCwEc = state(1, 23, [1,0,1,1],3) #25 neighbourB
		CrCrCwER = state(2, 23, [1,0,2,1],3) #25
		CrCrCwEe = state(2, 23, [1,0,2,1],3) #25
		
		
		
		CwCr = state(5, )
		
	end
	
end

# ╔═╡ a2a9d33b-ff11-4988-a9c4-47432f91d2eb
function start(c_State)
	
	#	currentBalance != 0 #Wont run if No Moves made by the agent yet
	T_state = state()
	
	#Temporary Values To build new States (nodes)
	T_position =  Current_position
	T_Balance = currentBalance
	T_CT = ItemCount
	location = Current_position.current_O.index
	
	for actionz in availableActions
			#Agent to Move West
		if actionz == MW && Current_position.current_O.neighbourW == true
		
		index = Current_position.current_O.index #Record Index
		
		T_position.prev_O = officeList[index] #Record Office Being Left
		
		T_position.prev_A = MW #Record Action taken
		
		T_position.current_O = officeList[index - 1] #Record Current 
		
		 # Change Location
		
		T_Balance = currentBalance.value + MW.Cost #add Cost
		
		push!(moves, Action.Name) #Track movement
			
			
		#Build/construct New State
		
		T_state.I_Count = T_CT
			
		T_state.S_Cost = T_Balance
			
		T_state.office_Lot = c_State.office_Lot
		
		T_state.A_position = T_position.current_O.index
			
		T_state.huristicVal = ItemCount  + T_Balance
		
		# for o in officeLot
		# 	push!(T_state.office_Lot, c_State.office_Lot)
		# end
			
			
		return
end
	
	end #End For Loop
	
	#Agent must Move East
		if Action.Name == "Move East" && Old_Location.current_O.neighbourE == true
#=
		if Old_Location.prev_A == Mw && Old_Location.prev_O.itemCount == 0 Old_Location.current_O.neighbourW == false
			push!(moves, "Usless Move East")
			return
		end
=#
		index = Old_Location.current_O.index
		Old_Location.prev_O = officeList[index]
		Old_Location.prev_A = Action
		Old_Location.current_O = officeList[index + 1]
		Current_position = Old_Location
		currentBalance.value = currentBalance.value + Action.Cost
		push!(moves, Action.Name) #Track movement
		push!(actionQue, Action.Name)
		#push!(moves, string("New Cost ", currentBalance.value))
		return

	elseif Action.Name == "Move East" && Old_Location.current_O.neighbourE == false
	push!(moves, string("East Move Not Allowed => ", Old_Location.current_O.index))
		return
end
	
	
	#Collect Action
		if Action.Name == "Collect_parcel"
	
		if  Old_Location.prev_A == Co
			push!(moves, "Cannot Collect Twice")
			return
	end
		
		if Old_Location.current_O.itemCount == 0
			push!(moves, "Office Is Empty")
			return
	end				
		
				    #If West Office
		if  Wo == Old_Location.current_O
			 Wo.itemCount = Wo.itemCount - 1;
				return
				end
		
					#If C1 Office
		if C1 == Old_Location.current_O
		     	C1.itemCount = C1.itemCount - 1;
				return
				end 
		
					#If C2 Office
		if C2 == Old_Location.current_O
				 C2.itemCount = C2.itemCount - 1;
				return
				end		
		
					#If East Office
		if E == Old_Location.current_O
				 E.itemCount = E.itemCount - 1;
				return
				end			
			
	#Remain Action
		if Action.Name == "Remain"
			
		if Old_Location.current_O.itemCount == 0
			push!(moves, "Cannot Remain In Empty Office")
			return
	end		
			
		if  Old_Location.prev_A == Re
			push!(moves, "Cannot Remain Twice")
			return
	end
			
				    #If West Office
		if  Wo == Old_Location.current_O
			 Wo.itemCount = Wo.itemCount - 1;
				return
				end
					#If C1 Office
		if C1 == Old_Location.current_O
		     	C1.itemCount = C1.itemCount - 1;
				return
				end 
					#If C2 Office
		if C2 == Old_Location.current_O
				 C2.itemCount = C2.itemCount - 1;
				return
				end		
					#If East Office
		if E == Old_Location.current_O
				 E.itemCount = E.itemCount - 1;
				return
				end	
			end
		end
	
	# currentBalance = 
	Current_position = T_position
	end

# Cell order:
# ╠═72641af2-af36-11eb-060b-377776bf04a4
# ╠═1d119b9b-138f-4465-b866-b7383aa69312
# ╟─286ec198-8005-44e5-b3fb-8e54f48b6709
# ╟─8b9602bc-9ef3-410a-9379-079e373cb375
# ╟─404acaa3-9b99-47f5-8279-4fa7fd2fdf74
# ╠═e834f65e-0db3-4f84-bfd4-f6650c65312a
# ╠═fdd6e435-cf61-4720-92d9-270f545baca1
# ╠═d4776e5f-5a53-479c-8965-331f91380f73
# ╠═6b7b89bc-4eb6-42e8-ac8c-fc9e05a04828
# ╠═805542fa-ff1e-4bc8-9a18-c6fe1ee91fda
# ╠═89b1320e-27cc-41d1-9028-4d641aa593b6
# ╠═6ab1f0f3-de2d-4b8b-93fb-185dc30e073c
# ╠═338783ab-1ea8-4956-883d-14d9a39af543
# ╠═62553898-4d5f-41a4-a07d-87db9f5b4519
# ╠═4e8d81dc-6e9f-42de-9ef9-24664ada11b5
# ╠═281ee45c-20ca-4e43-a885-7f78987acb44
# ╠═7cb1530a-4e45-4513-8f62-69ca5d33dc43
# ╠═307ffb1a-edfb-4bcf-a675-43315d01b700
# ╟─38cd1fc8-b6e8-4fab-a034-95b977bc3c27
# ╟─47ae0816-9368-43e3-b153-6cff476612ed
# ╟─b1e9d358-e009-46ad-8d31-f00af4d4cdae
# ╠═74950239-9b5e-4110-ae4f-31478770d8c5
# ╠═6cfc0bc9-3df6-48b5-aa6a-d7dabe937f87
# ╠═0781e581-78bd-4ea1-83f7-3573de6f0119
# ╠═4594f6b8-51e9-4749-91ce-ced5e73d2021
# ╠═18f79661-81ca-4299-a8f9-74a7b42ab6f5

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[deps]
"""

# ╔═╡ Cell order:
# ╠═2c1ec482-859a-46de-9abe-2569d8db5dda
# ╠═422c6542-db60-4a4c-a98e-a4dc6c84b769
# ╠═6e8b44f7-1b2f-40d0-a957-6bf77a9dd959
# ╠═ff220984-4000-41df-880e-4e17f9c20155
# ╠═b5dc583f-51e8-4216-b6c4-9c53f3f5301c
# ╠═e4386126-392d-425d-b22d-aa31cfb45d69
# ╠═ce1895fb-3816-4f17-9fe2-20a480b70a05
# ╠═cd8f1cd7-c636-475f-9abf-72e2fcf2a9f7
# ╠═d3561084-b63e-4010-bc01-a9af6b0ce415
# ╠═a56da88d-fe30-443d-b000-071b464f5e8e
# ╠═0c3405fd-eaa6-4e0d-93e8-fc94ce35db23
# ╠═ffe1431d-44ed-4fb2-812e-49c57bb8dbb1
# ╠═15b6d80b-dcc9-4bab-bd0d-b50bb64ea693
# ╠═ab785296-e53b-4d43-ae9c-c99ffb73ee7c
# ╠═2949aa02-4b6b-49d5-acfd-cb1847fe29b6
# ╠═b0f5ae21-fc63-47ce-872b-c2965e04ff7b
# ╠═559c1183-d067-4917-b1d6-960d4e21cead
# ╠═ad417b7e-9abf-4509-998a-6790f1676f09
# ╠═23f640b0-4b95-42f1-b5a8-2367351f4e47
# ╠═b1b1721f-ed6c-43a1-9229-05ca2fa1874c
# ╠═2bbfb355-4374-45df-941c-d674a32be9f4
# ╠═a2a9d33b-ff11-4988-a9c4-47432f91d2eb
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002